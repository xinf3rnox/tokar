import junit.framework.TestCase;
import net.pol.app.quarto.PlateauQuartoplus;

public class SaveTest extends TestCase {

    public void testLoadFileWithPiece() {
        String file = SaveTest.class.getResource("plateau2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        System.out.println(plateauQuartoplus.toString());
        plateauQuartoplus.saveToFile("files/outfile.txt");
    }
}
