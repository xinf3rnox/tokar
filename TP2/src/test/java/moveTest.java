import junit.framework.TestCase;
import net.pol.app.quarto.PlateauQuartoplus;
import org.junit.Assert;

public class moveTest extends TestCase {

    public void testMoveValide() {
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        boolean moveValide = plateauQuartoplus.estmoveValide("B2","noir");
        Assert.assertTrue(moveValide);
    }

    public void testMoveInvalide() {
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        boolean moveValide = plateauQuartoplus.estmoveValide("J2","noir");
        Assert.assertFalse(moveValide);
    }

    public void testMoveDejaUtilise() {
        String file = LoadingTest.class.getResource("plateau2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        boolean moveValide = plateauQuartoplus.estmoveValide("B1","noir");
        Assert.assertFalse(moveValide);
    }


    public void testMovePossibles() {
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        String[] movePossibles = plateauQuartoplus.mouvementsPossibles("noir");
        String ret= "";
        for(int i=0;i<movePossibles.length;i++){
            ret +=  movePossibles[i] + " ; ";
        }
        System.out.println(ret);
        Assert.assertEquals(movePossibles.length,16);
    }
}