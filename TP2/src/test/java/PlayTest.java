import junit.framework.TestCase;
import net.pol.app.quarto.PlateauQuartoplus;

public class PlayTest extends TestCase {

    public void testPosePiece() {
        String file = LoadingTest.class.getResource("plateau1.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        plateauQuartoplus.play("C2","bppr", "noir");
        plateauQuartoplus.play("D4","bgtr", "blanc");
        plateauQuartoplus.saveToFile("files/outfile.txt");
    }

    public void testPosePieceDejaPose() {
        String file = LoadingTest.class.getResource("plateau2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        plateauQuartoplus.play("D4","bppr", "noir");
    }

    public void testPosePieceSurUneAutrePiece() {
        String file = LoadingTest.class.getResource("plateau2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        plateauQuartoplus.play("B1","bgtr", "noir");
        plateauQuartoplus.saveToFile("files/outfile.txt");
    }

    public void testPosePieceQuiNexistePas() {
        String file = LoadingTest.class.getResource("plateau2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        plateauQuartoplus.play("C3","tpda", "noir");
        plateauQuartoplus.saveToFile("files/outfile.txt");
    }

    public void testPosePieceCaseNexistePas() {
        String file = LoadingTest.class.getResource("plateau2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        plateauQuartoplus.play("E3","bgtr", "noir");
        plateauQuartoplus.saveToFile("files/outfile.txt");
    }
}
