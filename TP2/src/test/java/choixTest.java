import junit.framework.TestCase;
import net.pol.app.quarto.PlateauQuartoplus;
import org.junit.Assert;

public class choixTest extends TestCase {

    public void testChoixValide() {
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        boolean choixValide = plateauQuartoplus.estchoixValide("bgpr", "noir");
        Assert.assertTrue(choixValide);
    }

    public void testChoixInvalide() {
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        boolean choixValide = plateauQuartoplus.estchoixValide("btpa", "noir");
        Assert.assertFalse(choixValide);
    }

    public void testChoixDejaPose() {
        String file = LoadingTest.class.getResource("plateau2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertFalse(plateauQuartoplus.estchoixValide("B1", "noir"));
    }


    public void testChoixPossibles() {
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        String[] choixPossibles = plateauQuartoplus.choixPossibles("noir");
        String ret = "";
        for (int i = 0; i < choixPossibles.length; i++) {
            ret += choixPossibles[i] + " ; ";
        }
        System.out.println(ret);
        System.out.println(choixPossibles.length);
        Assert.assertEquals(choixPossibles.length,16);
    }
}
