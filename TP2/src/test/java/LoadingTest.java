import junit.framework.TestCase;
import net.pol.app.quarto.PlateauQuartoplus;

public class LoadingTest extends TestCase {

    public void testLoadFile() {
        String file = LoadingTest.class.getResource("plateau1.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        System.out.println(plateauQuartoplus.toString());
    }

    public void testLoadFileWithPiece() {
        String file = LoadingTest.class.getResource("plateau2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        System.out.println(plateauQuartoplus.toString());
    }

    /**
     * Une piece qui n'existe pas
     */
    public void testLoadFileWithPieceInvalid() {
        String file = LoadingTest.class.getResource("fausse_piece.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        System.out.println(plateauQuartoplus.toString());
    }

    /**
     * deux fois la meme piece
     */
    public void testLoadFileWithDoublePiece() {
        String file = LoadingTest.class.getResource("double_piece.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        System.out.println(plateauQuartoplus.toString());
    }

    /**
     * un - a l'emplacement d'un +
     */
    public void testLoadFileMalFormer() {
        String file = LoadingTest.class.getResource("fausse_emplacement_vide.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        System.out.println(plateauQuartoplus.toString());
    }

    /**
     * ligne 4 a la place de la ligne 2
     */
    public void testLoadFileMalFormer2() {
        String file = LoadingTest.class.getResource("malformer.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        System.out.println(plateauQuartoplus.toString());
    }

    /**
     * ligne 8 qui n'existe pas
     */
    public void testLoadFileMalFormer3() {
        String file = LoadingTest.class.getResource("malformer2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        System.out.println(plateauQuartoplus.toString());
    }
}
