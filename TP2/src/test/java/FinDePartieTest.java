import junit.framework.TestCase;
import net.pol.app.quarto.PlateauQuartoplus;
import org.junit.Assert;

public class FinDePartieTest extends TestCase {

    public void testFinDePartieFalse() {
        String file = LoadingTest.class.getResource("plateau1.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        plateauQuartoplus.play("C2","bppr", "noir");
        plateauQuartoplus.play("D4","bgtr", "blanc");
        plateauQuartoplus.play("C3","rgpc", "noir");
        plateauQuartoplus.play("A1","rptc", "blanc");
        Assert.assertFalse(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartieDiag() {
        String file = LoadingTest.class.getResource("plateau_diag.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertTrue(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartiecol() {
        String file = LoadingTest.class.getResource("plateau_col.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertTrue(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartierow() {
        String file = LoadingTest.class.getResource("plateau_row.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertTrue(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartieCarre1() {
        String file = LoadingTest.class.getResource("plateau_carre1.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertTrue(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartieCarre2() {
        String file = LoadingTest.class.getResource("plateau_carre2.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertTrue(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartieNonDiag() {
        String file = LoadingTest.class.getResource("non_victoire_diag.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertFalse(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartieNonCol() {
        String file = LoadingTest.class.getResource("non_victoire_colonne.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertFalse(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartieNonCarre() {
        String file = LoadingTest.class.getResource("non_victoire_carre.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertFalse(plateauQuartoplus.finDePartie());
    }

    public void testFinDePartieNonRow() {
        String file = LoadingTest.class.getResource("non_victoire_ligne.txt").getFile();
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        plateauQuartoplus.setFromFile(file);
        Assert.assertFalse(plateauQuartoplus.finDePartie());
    }
}
