package net.pol.app.quarto;

public interface Partie1 {

    /** initialise un plateau `a partir d’un fichier texte
     * @param fileName le nom du fichier `a lire
     */
    void setFromFile(String fileName);
    /** sauve la configuration de l’´etat courant (plateau et pi`eces restantes) dans un fichier
     * @param fileName le nom du fichier `a sauvegarder
     * Le format doit ^etre compatible avec celui utilis´e pour la lecture.
     */
    void saveToFile(String fileName);
    /** indique si le coup <choose> est valide pour le joueur <player> sur le plateau courant
     * @param choose le choix de la pi`ece sous la forme "bgpr"
     * @param player le joueur qui joue (repr´esent´e par "noir" ou "blanc")
     */
    boolean estchoixValide(String choose, String player);
    /** indique si le coup <move> est valide pour le joueur <player> sur le plateau courant
     * @param move le coup `a jouer sous la forme "B2"
     * @param player le joueur qui joue (repr´esent´e par "noir" ou "blanc")
     */
    boolean estmoveValide(String move, String player);
    /** calcule les coups possibles pour le joueur <player> sur le plateau courant
     * @param player le joueur qui joue (repr´esent´e par "noir" ou "blanc")
     */
    String[] mouvementsPossibles(String player);
    /** calcule les choix possibles pour le joueur <player> sur les pi`eces restantes
     * @param player le joueur qui joue (repr´esent´e par "noir" ou "blanc")
     */
    String[] choixPossibles(String player);
    /** modifie le plateau en jouant le coup move avec la pi`ece choose
     * @param move le coup `a jouer, repr´esent´e sous la forme "B2"
     * @param choose le choix de la pi`ece sous la forme "bgpr"
     * @param player le joueur qui joue repr´esent´e par "noir" ou "blanc"
     */
    void play(String move, String choose, String player);
    /** vrai lorsque le plateau corespond `a une fin de partie
     */
    boolean finDePartie();
}
