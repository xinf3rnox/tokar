package net.pol.app.quarto.enums;

public enum Sommet {
    PLEIN("p"),
    TROUE("t");

    private String name = "";

    //Constructeur
    Sommet(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
