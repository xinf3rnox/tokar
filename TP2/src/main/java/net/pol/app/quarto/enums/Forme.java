package net.pol.app.quarto.enums;

public enum Forme {
    ROND("r"),
    CARRE("c");

    private String name = "";

    //Constructeur
    Forme(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
