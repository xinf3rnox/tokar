package net.pol.app.quarto;

import net.pol.app.quarto.enums.Couleur;
import net.pol.app.quarto.enums.Forme;
import net.pol.app.quarto.enums.Sommet;
import net.pol.app.quarto.enums.Taille;

/**
 * Classe qui represente une piece de jeu du QUARTO
 * elle possede 4 attributs :
 *
 * - TAILLE : Grand/petit </br>
 * - COULEUR : Bleu/rouge </br>
 * - SOMMET : Plein/troue </br>
 * - FORME : Rond/carre </br>
 */
public class Piece {

    Couleur couleur;
    Taille taille;
    Forme forme;
    Sommet sommet;

    public Piece(Couleur couleur, Taille taille, Sommet sommet, Forme forme) {
        this.couleur = couleur;
        this.taille = taille;
        this.forme = forme;
        this.sommet = sommet;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public Taille getTaille() {
        return taille;
    }

    public Forme getForme() {
        return forme;
    }

    public Sommet getSommet() {
        return sommet;
    }

    @Override
    public String toString() {
        return couleur.toString()+taille.toString()+sommet.toString()+forme.toString();
    }
}
