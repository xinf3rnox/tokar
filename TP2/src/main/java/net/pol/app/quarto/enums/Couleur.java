package net.pol.app.quarto.enums;

public enum Couleur {
    BLEU ("b"),
    ROUGE("r");

    private String name = "";

    //Constructeur
    Couleur(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
