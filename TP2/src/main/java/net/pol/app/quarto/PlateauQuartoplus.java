package net.pol.app.quarto;

import net.pol.app.quarto.enums.Couleur;
import net.pol.app.quarto.enums.Forme;
import net.pol.app.quarto.enums.Sommet;
import net.pol.app.quarto.enums.Taille;

import java.io.*;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

/**
 * Classe de notre partie de quarto !!!
 */
public class PlateauQuartoplus implements Partie1 {

    /*plateau[row][col]*/
    Piece[][] plateau;
    List<Piece> pieces;

    public PlateauQuartoplus() {
        resetGame();
    }

    private void resetGame() {
        plateau = new Piece[4][4];
        pieces = new ArrayList<Piece>();
        for (Couleur c : Couleur.values()) {
            for (Taille t : Taille.values()) {
                for (Sommet s : Sommet.values()) {
                    for (Forme f : Forme.values()) {
                        pieces.add(new Piece(c, t, s, f));
                    }
                }
            }
        }
        Collections.shuffle(pieces);
    }

    public void setFromFile(String fileName) {
        File file = new File(fileName);
        resetGame();
        try {
            // Reset du jeu (pieces et plateau)

            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null) {
                if (!st.startsWith("%")) {
                    if (st.matches("[1-4]\\s([a-zA-Z]{4}|\\+){4}\\s[1-4]")) {
                        int row = Integer.parseInt(String.valueOf(st.charAt(0))) - 1;
                        int col = 0;
                        int x = 2;
                        while (col < 4) {
                            if (st.charAt(x) != '+') {
                                String piece_name = new StringBuilder().append(st.charAt(x)).append(st.charAt(x + 1)).append(st.charAt(x + 2)).append(st.charAt(x + 3)).toString();
                                int index = findPieceInList(piece_name);
                                if (index == -1) {
                                    throw new QuartoException("La piece n'existe pas ou n'est pas dans la liste des pieces disponible");
                                } else {
                                    plateau[row][col] = pieces.get(index);
                                    pieces.remove(index);
                                    x = x + 3;
                                }
                            } else {
                                plateau[row][col] = null;
                            }
                            x += 1;
                            col = col + 1;
                        }
                    } else {
                        System.out.println("Cette n'est pas valide : " + st);
                        resetGame();
                    }
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("Le fichier n'existe pas ! ABORRRT MISSION !");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Allo un problème avec le fichier !");
            System.out.println(e.getMessage());
        } catch (QuartoException e) {
            System.out.println(e.getMessage());
        }
    }

    private int findPieceInList(String piece_name) {
        for (int x = 0; x < pieces.size(); x++) {
            if (piece_name.equals(pieces.get(x).toString())) {
                return x;
            }
        }
        return -1;
    }

    public void saveToFile(String fileName) {
        try {
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");

            writer.println("% Etat Initial du plateau de jeu:");
            writer.println("% ABCD");

            for (int x = 0; x < plateau.length; x++) {
                writer.print((x + 1) + " ");
                for (int y = 0; y < plateau[x].length; y++) {
                    if (plateau[x][y] != null) {
                        writer.print(plateau[x][y]);
                    } else {
                        writer.print("+");
                    }
                }
                writer.println(" " + (x + 1));
            }

            writer.println("% ABCD");
            writer.println("% By Arthur and Kevin");
            writer.close();
        } catch (Exception e) {
            System.out.println("erreur durant la sauvegarde, sauvegarde corrompue");
            System.out.println(e.getMessage());
        }
    }

    public boolean estchoixValide(String choose, String player) {
        if (findPieceInList(choose) == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean estmoveValide(String move, String player) {
        /*move = "B2"*/
        int col = -1;
        int row = -1;
        if (String.valueOf(move.charAt(0)).matches("[A-D]")) {
            col = move.charAt(0) - 'A';
        }
        if (String.valueOf(move.charAt(1)).matches("[1-4]")) {
            row = Integer.parseInt(String.valueOf(move.charAt(1))) - 1;
        }
        if (col != -1 && row != -1) {
            if (plateau[row][col] == null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public String[] mouvementsPossibles(String player) {
        List<String> ret = new ArrayList<String>();
        for (int x = 0; x < plateau.length; x++) {
            for (int y = 0; y < plateau[x].length; y++) {
                if (plateau[x][y] == null) {
                    String pos = "";
                    pos += String.valueOf((char) (y + 'A'));
                    pos += (x + 1) + "";
                    ret.add(pos);
                }
            }
        }
        return ret.toArray(new String[0]);
    }

    public String[] choixPossibles(String player) {
        return pieces.stream().map(Piece::toString).toArray(String[]::new);
    }

    public void play(String move, String choose, String player) {
        int col = move.charAt(0) - 'A';
        int row = Integer.parseInt(String.valueOf(move.charAt(1))) - 1;
        if (estmoveValide(move, player)) {
            if (estchoixValide(choose, player)) {
                int index_piece = findPieceInList(choose);
                Piece a_poser = this.pieces.get(index_piece);
                this.plateau[row][col] = a_poser;
                this.pieces.remove(index_piece);
            } else {
                System.out.println("La piece " + choose + " n'est pas disponible dans la liste des pieces disponible");
            }
        } else {
            if (col < 4 && row < 4) {
                System.out.println("Le choix : " + move + " n'est pas possible car la piece " + plateau[row][col] + " est deja presente sur cette case");
            } else {
                System.out.println("La case " + move + " n'existe pas");
            }
        }
    }

    public boolean finDePartie() {
        if (pieces.size() > 12) {
            return false;
        }
        if (pieces.size() == 0) {
            return true;
        }

        //Il existe 4 comparator (fonction lambda de java 8)
        //Si la piece est de la couleur voulu renvoie 1 sinon 0
        BiFunction<Piece, Object, Integer> compare_couleur = (piece, couleur) -> (piece.getCouleur() == couleur) ? 0 : 1;
        //Si la piece est de la forme voulu renvoie 1 sinon 0
        BiFunction<Piece, Object, Integer> compare_forme = (piece, forme) -> (piece.getForme() == forme) ? 0 : 1;
        //Si la piece est du sommet voulu renvoie 1 sinon 0
        BiFunction<Piece, Object, Integer> compare_sommet = (piece, sommet) -> (piece.getSommet() == sommet) ? 0 : 1;
        //Si la piece est de la taille voulu renvoie 1 sinon 0
        BiFunction<Piece, Object, Integer> compare_taille = (piece, taille) -> (piece.getTaille() == taille) ? 0 : 1;

        return calculateFinDePartie(compare_couleur, Couleur.BLEU) || calculateFinDePartie(compare_forme, Forme.ROND) || calculateFinDePartie(compare_sommet, Sommet.PLEIN) || calculateFinDePartie(compare_taille, Taille.GRAND);
    }

    /**
     * Methode qui va comparer la matrice de la partie avec un comparator (est-ce que les piece sont bleu ?) et recréer une matrice contenant 0,1 ou -10
     *
     * 0 correspond a si la piece n'est pas egal a l'element compare (n'est pas bleu) </br>
     * 1 correspond a si la piece est egal a l'element compare (est bleu)</br>
     * -10 correspond a si l'emplacement est vide
     *
     * On va ensuite calculer si la ligne est egal a 0 ou a 4 (si toute la ligne est bleu ou rouge)</br>
     * calculer si la colonne est egal a 0 ou a 4 (si toute la colonne est bleu ou rouge)</br>
     * Si les diagonales sont egal a 0 ou 4</br>
     * et parcourir la matrice en carre pour savoir un il y a un carre egal a 0 ou a 4
     *
     * De cette facon nous pouvons detecter une fin de partie.
     *
     * @param comparator
     * @param enums
     * @return
     */
    private boolean calculateFinDePartie(BiFunction<Piece, Object, Integer> comparator, Object enums) {
        int[][] result = new int[plateau.length][plateau[0].length];
        for (int x = 0; x < plateau.length; x++) {
            for (int y = 0; y < plateau[x].length; y++) {
                if (plateau[x][y] != null) {
                    result[x][y] = comparator.apply(plateau[x][y], enums);
                } else {
                    result[x][y] = -10;
                }
            }
        }
        for (int x = 0; x < plateau.length; x++) {
            if (IntStream.of(result[x]).sum() == 4 || IntStream.of(result[x]).sum() == 0) {
                return true;
            }
            int col_result = 0;
            for (int y = 0; y < plateau[x].length; y++) {
                col_result += result[y][x];
            }
            if (col_result == 0 || col_result == 4) {
                return true;
            }
        }

        for (int x = 0; x < plateau.length - 1; x++) {
            for (int y = 0; y < plateau[x].length - 1; y++) {
                int carre_result = result[x][y] + result[x][y + 1] + result[x + 1][y] + result[x + 1][y + 1];
                if (carre_result == 0 || carre_result == 4) {
                    return true;
                }
            }
        }

        int diag_1 = result[0][0] + result[1][1] + result[2][2] + result[3][3];
        int diag_2 = result[0][3] + result[1][2] + result[2][1] + result[3][0];

        return diag_1 == 4 || diag_1 == 0 || diag_2 == 4 || diag_2 == 0;
    }

    @Override
    public String toString() {
        String result = "pieces=" + pieces + System.lineSeparator();
        for (int x = 0; x < plateau.length; x++) {
            result += (x + 1) + " ";
            for (int y = 0; y < plateau[x].length; y++) {
                if (plateau[x][y] != null) {
                    result += plateau[x][y];
                } else {
                    result += "+";
                }
            }
            result += " " + (x + 1) + System.lineSeparator();
        }
        return result;
    }

    public static void main(String[] args) {
        // TEST JEU RANDOM
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        String[] joueur = {"blanc", "noir"};
        int compteur = 0;
        String joueurCourant = "";
        String pieceSelectionnee = "";
        while (!plateauQuartoplus.finDePartie()) {
            joueurCourant = joueur[compteur % 2];
            String[] mouvementsPossibles = plateauQuartoplus.mouvementsPossibles(joueurCourant);
            int mouvementAleatoire = (int) (Math.random() * ((mouvementsPossibles.length)));
            String[] choixPossibles = plateauQuartoplus.choixPossibles(joueurCourant);
            int choixAleatoire = (int) (Math.random() * ((choixPossibles.length)));
            if (compteur != 0) {
                String move = mouvementsPossibles[mouvementAleatoire];
                while (!plateauQuartoplus.estmoveValide(move, joueurCourant)) {
                    /** Normalement, on ne se retrouve jamais dans ce cas car le move est choisi dans les mouvementsPossibles
                     * Donc si on se retrouve dans ce cas de figure, alors notre méthode "mouvementsPossibles" est fausses
                     **/
                    System.out.println("mouvementsPossibles en erreur");
                    mouvementAleatoire = (int) (Math.random() * ((mouvementsPossibles.length)));
                    move = mouvementsPossibles[mouvementAleatoire];
                }
                plateauQuartoplus.play(move, pieceSelectionnee, joueurCourant);
                System.out.println(joueurCourant + " a joue " + pieceSelectionnee + " en " + move);
            }
            // On rappel les fonctions car les choix possibles ont été modifié apres que le joueur est joué son coup
            choixPossibles = plateauQuartoplus.choixPossibles(joueurCourant);
            choixAleatoire = (int) (Math.random() * ((choixPossibles.length)));
            String choix = choixPossibles[choixAleatoire];
            while (!plateauQuartoplus.estchoixValide(choix, joueurCourant)) {
                /** Normalement, on ne se retrouve jamais dans ce cas car la piece est choisie dans les choixPossibles
                 * Donc si on se retrouve dans ce cas de figure, alors notre méthode "choixPossibles" est fausses
                 **/
                System.out.println("piecesPossibles en erreur ");
                choixAleatoire = (int) (Math.random() * ((choixPossibles.length)));
                choix = choixPossibles[choixAleatoire];
            }
            pieceSelectionnee = choix;
            compteur++;
        }
        if (plateauQuartoplus.choixPossibles(joueurCourant).length == 0) {
            // C'est un match nul
            System.out.println("C'est un match null");
        } else {
            System.out.println(joueurCourant + " a gagne");
        }
        plateauQuartoplus.saveToFile("files/outfile.txt");
    }
}
