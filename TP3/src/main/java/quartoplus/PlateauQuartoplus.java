package quartoplus;

import javafx.util.Pair;
import quartoplus.enums.Couleur;
import quartoplus.enums.Forme;
import quartoplus.enums.Sommet;
import quartoplus.enums.Taille;

import java.io.*;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

/**
 * Classe de notre partie de quarto !!!
 */
public class PlateauQuartoplus implements Partie1 {

    /*plateau[row][col]*/
    private Piece[][] plateau;
    private List<Piece> pieces;
    private int bleu = 8;
    private int rouge = 8;
    private int petit = 8;
    private int grand = 8;
    private int plein = 8;
    private int troue = 8;
    private int rond = 8;
    private int carre = 8;
    private Integer[] tailleAlign = {-1, -1};
    private Integer[] formeAlign = {-1, -1};
    private Integer[] colorAlign = {-1, -1};
    private Integer[] sommetAlign = {-1, -1};

    public PlateauQuartoplus() {
        resetGame();
    }

    public PlateauQuartoplus(Piece[][] newPlateau, List<Piece> newList, int bleu, int rouge, int petit, int grand, int plein, int troue, int rond, int carre) {
        plateau = newPlateau;
        pieces = newList;
        this.rond = rond;
        this.grand = grand;
        this.petit = petit;
        this.bleu = bleu;
        this.rouge = rouge;
        this.plein = plein;
        this.troue = troue;
        this.carre = carre;
    }

    private void resetGame() {
        plateau = new Piece[4][4];
        pieces = new ArrayList<Piece>();
        for (Couleur c : Couleur.values()) {
            for (Taille t : Taille.values()) {
                for (Sommet s : Sommet.values()) {
                    for (Forme f : Forme.values()) {
                        pieces.add(new Piece(c, t, s, f));
                    }
                }
            }
        }
        Collections.shuffle(pieces);
    }

    public void setFromFile(String fileName) {
        File file = new File(fileName);
        resetGame();
        try {
            // Reset du jeu (pieces et plateau)

            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null) {
                if (!st.startsWith("%")) {
                    if (st.matches("[1-4]\\s([a-zA-Z]{4}|\\+){4}\\s[1-4]")) {
                        int row = Integer.parseInt(String.valueOf(st.charAt(0))) - 1;
                        int col = 0;
                        int x = 2;
                        while (col < 4) {
                            if (st.charAt(x) != '+') {
                                String piece_name = new StringBuilder().append(st.charAt(x)).append(st.charAt(x + 1)).append(st.charAt(x + 2)).append(st.charAt(x + 3)).toString();
                                int index = findPieceInList(piece_name);
                                if (index == -1) {
                                    throw new QuartoException("La piece n'existe pas ou n'est pas dans la liste des pieces disponible");
                                } else {
                                    Piece a_poser = pieces.get(index);
                                    plateau[row][col] = a_poser;
                                    this.posePieceCalculate(a_poser);
                                    pieces.remove(index);
                                    x = x + 3;
                                }
                            } else {
                                plateau[row][col] = null;
                            }
                            x += 1;
                            col = col + 1;
                        }
                    } else {
                        System.out.println("Cette n'est pas valide : " + st);
                        resetGame();
                    }
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("Le fichier n'existe pas ! ABORRRT MISSION !");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Allo un problème avec le fichier !");
            System.out.println(e.getMessage());
        } catch (QuartoException e) {
            System.out.println(e.getMessage());
        }
    }

    private int findPieceInList(String piece_name) {
        for (int x = 0; x < pieces.size(); x++) {
            if (piece_name.equals(pieces.get(x).toString())) {
                return x;
            }
        }
        return -1;
    }

    public void saveToFile(String fileName) {
        try {
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");

            writer.println("% Etat Initial du plateau de jeu:");
            writer.println("% ABCD");

            for (int x = 0; x < plateau.length; x++) {
                writer.print((x + 1) + " ");
                for (int y = 0; y < plateau[x].length; y++) {
                    if (plateau[x][y] != null) {
                        writer.print(plateau[x][y]);
                    } else {
                        writer.print("+");
                    }
                }
                writer.println(" " + (x + 1));
            }

            writer.println("% ABCD");
            writer.println("% By Arthur and Kevin");
            writer.close();
        } catch (Exception e) {
            System.out.println("erreur durant la sauvegarde, sauvegarde corrompue");
            System.out.println(e.getMessage());
        }
    }

    public boolean estchoixValide(String choose) {
        if (findPieceInList(choose) == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean estmoveValide(String move) {
        /*move = "B2"*/
        int col = -1;
        int row = -1;
        if (String.valueOf(move.charAt(0)).matches("[A-D]")) {
            col = move.charAt(0) - 'A';
        }
        if (String.valueOf(move.charAt(1)).matches("[1-4]")) {
            row = Integer.parseInt(String.valueOf(move.charAt(1))) - 1;
        }
        if (col != -1 && row != -1) {
            if (plateau[row][col] == null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public String[] mouvementsPossibles() {
        List<String> ret = new ArrayList<String>();
        for (int x = 0; x < plateau.length; x++) {
            for (int y = 0; y < plateau[x].length; y++) {
                if (plateau[x][y] == null) {
                    String pos = "";
                    pos += String.valueOf((char) (y + 'A'));
                    pos += (x + 1) + "";
                    ret.add(pos);
                }
            }
        }
        return ret.toArray(new String[0]);
    }

    public String[] choixPossibles() {
        return pieces.stream().map(Piece::toString).toArray(String[]::new);
    }

    public void play(String move, String choose) {
        int col = move.charAt(0) - 'A';
        int row = Integer.parseInt(String.valueOf(move.charAt(1))) - 1;
        if (estmoveValide(move)) {
            if (estchoixValide(choose)) {
                int index_piece = findPieceInList(choose);
                Piece a_poser = this.pieces.get(index_piece);
                this.plateau[row][col] = a_poser;
                this.pieces.remove(index_piece);
                this.posePieceCalculate(a_poser);
            } else {
                System.out.println("La piece " + choose + " n'est pas disponible dans la liste des pieces disponible");
            }
        } else {
            if (col < 4 && row < 4) {
                System.out.println("Le choix : " + move + " n'est pas possible car la piece " + plateau[row][col] + " est deja presente sur cette case");
            } else {
                System.out.println("La case " + move + " n'existe pas");
            }
        }
    }

    private void posePieceCalculate(Piece piece) {
        if (piece.couleur == Couleur.BLEU) {
            bleu--;
        } else {
            rouge--;
        }
        if (piece.taille == Taille.PETIT) {
            petit--;
        } else {
            grand--;
        }
        if (piece.sommet == Sommet.PLEIN) {
            plein--;
        } else {
            troue--;
        }
        if (piece.forme == Forme.ROND) {
            rond--;
        } else {
            carre--;
        }
    }

    public boolean finDePartie() {
        if (pieces.size() > 12) {
            return false;
        }
        if (pieces.size() == 0) {
            return true;
        }

        return getNbColorAlign()[0] == 4 || getNbColorAlign()[1] == 4 || getNbFormeAlign()[0] == 4 || getNbFormeAlign()[1] == 4 || getNbSommetAlign()[0] == 4 || getNbSommetAlign()[1] == 4 || getNbTailleAlign()[0] == 4 || getNbTailleAlign()[1] == 4;
    }

    public Integer[] getNbColorAlign() {
        if (colorAlign[0] != -1) {
            return colorAlign;
        } else {
            BiFunction<Piece, Object, Integer> compare_couleur = (piece, couleur) -> (piece.getCouleur() == couleur) ? 1 : -1;
            colorAlign = calculateAlignement(compare_couleur, Couleur.BLEU);
            return colorAlign;
        }
    }

    public Integer[] getNbFormeAlign() {
        if (formeAlign[0] != -1) {
            return formeAlign;
        } else {
            BiFunction<Piece, Object, Integer> compare_forme = (piece, forme) -> (piece.getForme() == forme) ? 1 : -1;
            formeAlign = calculateAlignement(compare_forme, Forme.ROND);
            return formeAlign;
        }
    }

    public Integer[] getNbSommetAlign() {
        if (sommetAlign[0] != -1) {
            return sommetAlign;
        } else {
            //Si la piece est du sommet voulu renvoie 1 sinon 0
            BiFunction<Piece, Object, Integer> compare_sommet = (piece, sommet) -> (piece.getSommet() == sommet) ? 1 : -1;
            sommetAlign = calculateAlignement(compare_sommet, Sommet.PLEIN);
            return sommetAlign;
        }
    }

    public Integer[] getNbTailleAlign() {
        if (tailleAlign[0] != -1) {
            return tailleAlign;
        } else {
            //Si la piece est de la taille voulu renvoie 1 sinon 0
            BiFunction<Piece, Object, Integer> compare_taille = (piece, taille) -> (piece.getTaille() == taille) ? 1 : -1;
            tailleAlign = calculateAlignement(compare_taille, Taille.PETIT);
            return tailleAlign;
        }
    }

    /**
     * Methode qui va comparer la matrice de la partie avec un comparator (est-ce que les piece sont bleu ?) et recréer une matrice contenant 0,1 ou -10
     * <p>
     * 0 correspond a si la piece n'est pas egal a l'element compare (n'est pas bleu) </br>
     * 1 correspond a si la piece est egal a l'element compare (est bleu)</br>
     * -10 correspond a si l'emplacement est vide
     * <p>
     * On va ensuite calculer si la ligne est egal a 0 ou a 4 (si toute la ligne est bleu ou rouge)</br>
     * calculer si la colonne est egal a 0 ou a 4 (si toute la colonne est bleu ou rouge)</br>
     * Si les diagonales sont egal a 0 ou 4</br>
     * et parcourir la matrice en carre pour savoir un il y a un carre egal a 0 ou a 4
     * <p>
     * De cette facon nous pouvons detecter une fin de partie.
     *
     * @param comparator
     * @param enums
     * @return
     */
    public Integer[] calculateAlignement(BiFunction<Piece, Object, Integer> comparator, Object enums) {
        int[][] result = new int[plateau.length][plateau[0].length];
        Integer[] nbononeline = new Integer[]{0, 0};

        for (int x = 0; x < plateau.length; x++) {
            for (int y = 0; y < plateau[x].length; y++) {
                if (plateau[x][y] != null) {
                    result[x][y] = comparator.apply(plateau[x][y], enums);
                } else {
                    result[x][y] = -10;
                }
            }
        }
        for (int x = 0; x < plateau.length; x++) {
            calculate(IntStream.of(result[x]).sum(), nbononeline);
            int col_result = 0;
            for (int y = 0; y < plateau[x].length; y++) {
                col_result += result[y][x];
            }
            calculate(col_result, nbononeline);
        }

        for (int x = 0; x < plateau.length - 1; x++) {
            for (int y = 0; y < plateau[x].length - 1; y++) {
                int carre_result = result[x][y] + result[x][y + 1] + result[x + 1][y] + result[x + 1][y + 1];
                calculate(carre_result, nbononeline);
            }
        }

        int diag_1 = result[0][0] + result[1][1] + result[2][2] + result[3][3];
        int diag_2 = result[0][3] + result[1][2] + result[2][1] + result[3][0];

        calculate(diag_1, nbononeline);
        calculate(diag_2, nbononeline);

        return nbononeline;
    }

    private void calculate(int x, Integer[] pairs) {
        switch (x) {
            case 4:
            case -4:
                pairs[0] = 4;
                pairs[1] = 4;
                break;
            case -18:
                pairs[0] = pairs[0] < 2 ? 2 : pairs[0];
                break;
            case -22:
                pairs[1] = pairs[1] < 2 ? 2 : pairs[1];
                break;
            case -7:
                pairs[0] = pairs[0] < 3 ? 3 : pairs[0];
                break;
            case -13:
                pairs[1] = pairs[1] < 3 ? 3 : pairs[1];
                break;
        }
    }

    public PlateauQuartoplus copy() {
        Piece[][] copy = new Piece[plateau.length][plateau.length];
        for (int x = 0; x < plateau.length; x++) {
            for (int y = 0; y < plateau[x].length; y++) {
                if (plateau[x][y] != null) {
                    copy[x][y] = plateau[x][y].clone();
                }
            }
        }
        ArrayList<Piece> pieces = new ArrayList<>();
        for (Piece piece : this.pieces) {
            pieces.add(piece.clone());
        }
        return new PlateauQuartoplus(copy, pieces, bleu, rouge, petit, grand, plein, troue, rond, carre);
    }

    @Override
    public String toString() {
        String result = "pieces=" + pieces + System.lineSeparator();
        for (int x = 0; x < plateau.length; x++) {
            result += (x + 1) + " ";
            for (int y = 0; y < plateau[x].length; y++) {
                if (plateau[x][y] != null) {
                    result += plateau[x][y];
                } else {
                    result += "+";
                }
            }
            result += " " + (x + 1) + System.lineSeparator();
        }
        return result;
    }

    public int getBleu() {
        return bleu;
    }

    public int getPetit() {
        return petit;
    }

    public int getPlein() {
        return plein;
    }

    public int getRond() {
        return rond;
    }

    public int getRouge() {
        return rouge;
    }

    public int getGrand() {
        return grand;
    }

    public int getTroue() {
        return troue;
    }

    public int getCarre() {
        return carre;
    }
}
