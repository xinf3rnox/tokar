package quartoplus;

public interface Partie1 {

    /** initialise un plateau `a partir d’un fichier texte
     * @param fileName le nom du fichier `a lire
     */
    void setFromFile(String fileName);
    /** sauve la configuration de l’´etat courant (plateau et pi`eces restantes) dans un fichier
     * @param fileName le nom du fichier `a sauvegarder
     * Le format doit ^etre compatible avec celui utilis´e pour la lecture.
     */
    void saveToFile(String fileName);
    /** indique si le coup <choose> est valide pour le joueur <player> sur le plateau courant
     * @param choose le choix de la pi`ece sous la forme "bgpr"
     */
    boolean estchoixValide(String choose);
    /** indique si le coup <move> est valide pour le joueur <player> sur le plateau courant
     * @param move le coup `a jouer sous la forme "B2"
     */
    boolean estmoveValide(String move);
    /** calcule les coups possibles pour le joueur <player> sur le plateau courant
     */
    String[] mouvementsPossibles();
    /** calcule les choix possibles pour le joueur <player> sur les pi`eces restantes
     */
    String[] choixPossibles();
    /** modifie le plateau en jouant le coup move avec la pi`ece choose
     * @param move le coup `a jouer, repr´esent´e sous la forme "B2"
     * @param choose le choix de la pi`ece sous la forme "bgpr"
     */
    void play(String move, String choose);
    /** vrai lorsque le plateau corespond `a une fin de partie
     */
    boolean finDePartie();
}
