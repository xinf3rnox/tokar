package quartoplus.enums;

public enum Taille {
    PETIT("p"),
    GRAND("g");

    private String name = "";

    //Constructeur
    Taille(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
