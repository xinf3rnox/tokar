package quartoplus.algo;

import quartoplus.Piece;
import quartoplus.PlateauQuartoplus;

public interface AlgoJeu {

    /** Renvoie le meilleur placement pour une piece
     * @param plateau le plateau de jeu
     * @param piece la piece a pose
     * @return
     */
	public String searchBestMoveAndPiece(PlateauQuartoplus plateau, String piece);


}
 