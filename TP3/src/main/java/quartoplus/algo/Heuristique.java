package quartoplus.algo;

import quartoplus.PlateauQuartoplus;

public interface Heuristique {

	public int eval(PlateauQuartoplus p, String move, String piece, String PieceDonner);

}
 