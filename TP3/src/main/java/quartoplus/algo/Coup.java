package quartoplus.algo;

public class Coup {
    public int value;
    public String move;
    public String piece;

    public Coup(){

    }

    public Coup(int value, String move, String piece) {
        this.value = value;
        this.move = move;
        this.piece = piece;
    }
}
