package quartoplus.algo;

import quartoplus.Piece;
import quartoplus.PlateauQuartoplus;
import quartoplus.enums.Couleur;
import quartoplus.enums.Forme;
import quartoplus.enums.Sommet;
import quartoplus.enums.Taille;

import java.util.function.BiFunction;


public class HeuristiquesQuarto {

    public static Heuristique hpiece = new Heuristique() {

        public int eval(PlateauQuartoplus p, String move, String putting, String toput) {

            if (p.finDePartie()) {
                return -10000000;
            }

            PlateauQuartoplus p2 = p.copy();

            p2.play(move, putting);

            int heuristique = calculateMatrix(p2);

            Integer[] bleu = p2.getNbColorAlign();
            Integer[] rond = p2.getNbFormeAlign();
            Integer[] plein = p2.getNbSommetAlign();
            Integer[] petit = p2.getNbTailleAlign();

            if (bleu[0] == 4 || bleu[1] == 4 || rond[0] == 4 || rond[1] == 4 || plein[0] == 4 || plein[1] == 4 || petit[0] == 4 || petit[1] == 4) {
                return 10000000;
            }

            //Comptait opposé restant ROUGE
            if (String.valueOf(toput.charAt(0)).equals(Couleur.BLEU.toString())) {
                if (bleu[0] == 3) {
                    return -10000000;
                }
                if (bleu[0] == 2) {
                    heuristique += p.getRouge() % 2 == 0 ? 1000 : -1000;
                }
            } else {
                if (bleu[1] == 3) {
                    return -10000000;
                }
                if (bleu[1] == 2) {
                    heuristique += p.getBleu() % 2 == 0 ? 1000 : -1000;
                }
            }

            if (String.valueOf(toput.charAt(1)).equals(Taille.PETIT.toString())) {
                if (petit[0] == 3) {
                    return -10000000;
                }
                if (petit[0] == 2) {
                    heuristique += p.getGrand() % 2 == 0 ? 1000 : -1000;
                }
            } else {
                if (petit[1] == 3) {
                    return -10000000;
                }
                if (petit[1] == 2) {
                    heuristique += p.getPetit() % 2 == 0 ? 1000 : -1000;
                }
            }

            if (String.valueOf(toput.charAt(2)).equals(Sommet.PLEIN.toString())) {
                if (plein[0] == 3) {
                    return -10000000;
                }
                if (plein[0] == 2) {
                    heuristique += p.getTroue() % 2 == 0 ? 1000 : -1000;
                }
            } else {
                if (plein[1] == 3) {
                    return -10000000;
                }
                if (plein[1] == 2) {
                    heuristique += p.getPlein() % 2 == 0 ? 1000 : -1000;
                }
            }

            if (String.valueOf(toput.charAt(3)).equals(Forme.ROND.toString())) {
                if (rond[0] == 3) {
                    return -10000000;
                }
                if (rond[0] == 2) {
                    heuristique += p.getCarre() % 2 == 0 ? 1000 : -1000;
                }
            } else {
                if (rond[1] == 3) {
                    return -10000000;
                }
                if (rond[1] == 2) {
                    heuristique += p.getRond() % 2 == 0 ? 1000 : -1000;
                }
            }
            return heuristique;
        }

        private int calculateMatrix(PlateauQuartoplus p) {
            int heuristique = 0;

            Integer[] bleu = p.getNbColorAlign();
            Integer[] rond = p.getNbFormeAlign();
            Integer[] plein = p.getNbSommetAlign();
            Integer[] petit = p.getNbTailleAlign();

            if (bleu[0] == 4 || bleu[1] == 4 || rond[0] == 4 || rond[1] == 4 || plein[0] == 4 || plein[1] == 4 || petit[0] == 4 || petit[1] == 4) {
                return 10000000;
            }

            if (bleu[0] == 3) {
                heuristique += p.getRouge() % 2 == 0 ? -1000 : 1000;
            }
            if (bleu[0] == 2) {
                heuristique += p.getRouge() % 2 == 0 ? 10 : -10;
            }
            if (bleu[1] == 3) {
                heuristique += p.getBleu() % 2 == 0 ? -1000 : 1000;
            }
            if (bleu[1] == 2) {
                heuristique += p.getBleu() % 2 == 0 ? 10 : -10;
            }

            if (petit[0] == 3) {
                heuristique += p.getGrand() % 2 == 0 ? -1000 : 1000;
            }
            if (petit[0] == 2) {
                heuristique += p.getGrand() % 2 == 0 ? 10 : -10;
            }
            if (petit[1] == 3) {
                heuristique += p.getPetit() % 2 == 0 ? -1000 : 1000;
            }
            if (petit[1] == 2) {
                heuristique += p.getPetit() % 2 == 0 ? 10 : -10;
            }

            if (plein[0] == 3) {
                heuristique += p.getTroue() % 2 == 0 ? -1000 : 1000;
            }
            if (plein[0] == 2) {
                heuristique += p.getTroue() % 2 == 0 ? 10 : -10;
            }
            if (plein[1] == 3) {
                heuristique += p.getPlein() % 2 == 0 ? -1000 : 1000;
            }
            if (plein[1] == 2) {
                heuristique += p.getPlein() % 2 == 0 ? 10 : -10;
            }

            if (rond[0] == 3) {
                heuristique += p.getCarre() % 2 == 0 ? -1000 : 1000;
            }
            if (rond[0] == 2) {
                heuristique += p.getCarre() % 2 == 0 ? 10 : -10;
            }
            if (rond[1] == 3) {
                heuristique += p.getRond() % 2 == 0 ? -1000 : 1000;
            }
            if (rond[1] == 2) {
                heuristique += p.getRond() % 2 == 0 ? 10 : -10;
            }

            return heuristique;
        }
    };

}
