/**
 *
 */

package quartoplus.algo;

import java.util.ArrayList;
import java.util.Arrays;

import javafx.util.Pair;
import quartoplus.Piece;
import quartoplus.PlateauQuartoplus;

public class MinimaxAB implements AlgoJeu {

    /**
     * L'heuristique utilisée par l'algorithme
     */
    private Heuristique hpiece = HeuristiquesQuarto.hpiece;

    private long tempsRestant = 300000;

    private double[] timeMax = {2.0, 2.0, 2.0, 2.0, 20.0, 20.0, 45.0, 45.0, 70.0, 70.0, 70.0, 70.0, 50.0, 50.0, 70.0, 70.0};
    private int[] depthMax = {1, 1, 1, 1, 3, 3, 5, 5, 5, 5, 3, 3, 3, 3, 3, 3};

    private long timeOut;

    // -------------------------------------------
    // Méthodes internes
    // -------------------------------------------

    @Override
    public String searchBestMoveAndPiece(PlateauQuartoplus plateau, String piece) {
        long timeStart = System.currentTimeMillis();
        this.timeOut = timeStart + (long)((this.timeMax[16 - plateau.choixPossibles().length] / 100.0) * this.tempsRestant);
        String pieceMove = searchBestMoveAndPieceWithTimer(plateau, piece);
        long timeStop = System.currentTimeMillis();
        if(this.timeOut < timeStop){
            System.out.println("LOLOLOL on a timeout");
        }
        this.tempsRestant = this.tempsRestant - (timeStop - timeStart);
        return pieceMove;
    }

    public String searchBestMoveAndPieceWithTimer(PlateauQuartoplus plateau, String piece) {
        if (piece == null) {
            return "A1-" + plateau.choixPossibles()[0];
        }
        if(plateau.mouvementsPossibles().length == 1){
            return plateau.mouvementsPossibles()[0] + "-xxxx";
        }
        return searchBestMoveAndPiece(plateau, piece, this.depthMax[16 - plateau.choixPossibles().length]);
    }

    public String searchBestMoveAndPiece(PlateauQuartoplus plateau, String piece, int depth) {
        Coup p = negab(plateau, "", "", piece, -10000000, 10000000, depth);
        return p.move + "-" + p.piece;
    }

    public Coup negab(PlateauQuartoplus plateau, String move, String piece, String pieceDonner, int alpha, int beta, int depth) {
        Coup retour = new Coup();
        if (plateau.finDePartie() || depth <= 0 || this.calculateTimeOut()) {
            return new Coup(-this.hpiece.eval(plateau, move, piece, pieceDonner), move, pieceDonner);
        }
        PlateauQuartoplus p2 = plateau.copy();
        if (!move.equals("") && !piece.equals("")) {
            p2.play(move, piece);
            if (p2.finDePartie()) {
                return new Coup(-10000000, move, pieceDonner);
            }
        }
        int alpharesult = alpha;
        for (String newMove : p2.mouvementsPossibles()) {
            int alphatmp = alpha;
            Coup retourtmp = new Coup();
            for (String newPieceGive : p2.choixPossibles()) {
                if (!newPieceGive.equals(pieceDonner)) {
                    Coup val = negab(p2, newMove, pieceDonner, newPieceGive, -beta, -alphatmp, depth - 1);
                    val.value = -val.value;
                    if (val.value >= alphatmp) {
                        alphatmp = val.value;
                        retourtmp.value = val.value;
                        retourtmp.move = newMove;
                        retourtmp.piece = newPieceGive;
                        if (alphatmp >= beta) {
                            break;
                        }
                    }
                }
            }
            if (alphatmp >= alpharesult) {
                alpharesult = alphatmp;
                retour.move = retourtmp.move;
                retour.piece = retourtmp.piece;
                retour.value = retourtmp.value;
                if(alpharesult >= beta){
                    return retour;
                }
            }
        }
        return retour;
    }

    private boolean calculateTimeOut(){
        long time =  System.currentTimeMillis();
        return this.timeOut < time;
    }
}