package quartoplus;

public class QuartoException extends Exception {

    public QuartoException(String message) { super(message); }
    public QuartoException(String message, Throwable cause) { super(message, cause); }


}
