package quartoplus;

import quartoplus.algo.MinimaxAB;

public class JoueurSuperFort implements IJoueur {

    int mycolor;
    PlateauQuartoplus plateauQuartoplus;
    String next_piece;
    MinimaxAB minimax;

    @Override
    public void initJoueur(int mycolour) {
        this.mycolor = mycolour;
        plateauQuartoplus = new PlateauQuartoplus();
        minimax = new MinimaxAB();
    }

    @Override
    public int getNumJoueur() {
        return this.mycolor;
    }

    @Override
    public String choixMouvement() {
        //IA MAGEULE
        System.out.println("C'est a moi de jouer");
        long startTime = System.currentTimeMillis();

        String move = this.minimax.searchBestMoveAndPiece(plateauQuartoplus,this.next_piece);
        String[] moves = move.split("-");
        if(this.next_piece != null){
            plateauQuartoplus.play(moves[0],this.next_piece);
        }

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println(String.format("Le tour a duré %s ms",elapsedTime));
        System.out.println(String.format("J'ai décidé de jouer %s", move));
        System.out.println(plateauQuartoplus);

        this.next_piece = moves[1];
        return move;
    }

    @Override
    public void declareLeVainqueur(int colour) {
        if(colour == this.mycolor){
            System.out.println("Quand il y a Roquefort, Il y a pas plus fort !");
            System.out.println("J'invoque exodia le maudit !");
            System.out.println("Tu ne le sais pas encore, mais tu es deja mort !");
        } else {
            System.out.println("Nan ton dragon blanc aux yeux bleus est trop fort !!!!");
            System.out.println("Je l'aurais un jour, je l'aurai");
            System.out.println("NANNNIII");
            System.out.println("Gomen nasai");
        }
    }

    @Override
    public void mouvementEnnemi(String coup) {
        System.out.println(String.format("L'ennemi joue %s",coup));
        String[] moves = coup.split("-");
        if(this.next_piece != null){
            plateauQuartoplus.play(moves[0],this.next_piece);
        }
        if(moves.length == 2){
            this.next_piece = moves[1];
        }
        System.out.println(plateauQuartoplus);
    }

    @Override
    public String binoName() {
        return "JEAN Kévin #Genious, YEN Arthur #BoGoss";
    }

    public void SetPlateau(PlateauQuartoplus plateau){
        this.plateauQuartoplus = plateau;
    }
}
