import junit.framework.TestCase;
import org.junit.Assert;
import quartoplus.JoueurSuperFort;
import quartoplus.PlateauQuartoplus;

public class PlateauTest extends TestCase {

    public void test(){
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        JoueurSuperFort joueurSuperFort = new JoueurSuperFort();
        joueurSuperFort.initJoueur(1);
        plateauQuartoplus.setFromFile(PlateauTest.class.getResource("double_piece2.txt").getFile());
        joueurSuperFort.SetPlateau(plateauQuartoplus);
        joueurSuperFort.mouvementEnnemi("D3-rgtc");
        String move = joueurSuperFort.choixMouvement();
        System.out.println(move);
        Assert.assertTrue(move.charAt(6) == 'c');
    }

    public void test2(){
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        JoueurSuperFort joueurSuperFort = new JoueurSuperFort();
        joueurSuperFort.initJoueur(1);
        plateauQuartoplus.setFromFile(PlateauTest.class.getResource("double_piece3.txt").getFile());
        joueurSuperFort.SetPlateau(plateauQuartoplus);
        joueurSuperFort.mouvementEnnemi("D3-bgtr");
        String move = joueurSuperFort.choixMouvement();
        System.out.println(move);
        Assert.assertEquals("C2-xxxx", move);
    }

    public void test3(){
        PlateauQuartoplus plateauQuartoplus = new PlateauQuartoplus();
        JoueurSuperFort joueurSuperFort = new JoueurSuperFort();
        joueurSuperFort.initJoueur(1);
        plateauQuartoplus.setFromFile(PlateauTest.class.getResource("double_piece.txt").getFile());
        joueurSuperFort.SetPlateau(plateauQuartoplus);
        joueurSuperFort.mouvementEnnemi("D3-rgtr");
        String move = joueurSuperFort.choixMouvement();
        System.out.println(move);
        Assert.assertTrue(move.startsWith("A1"));
    }
}
